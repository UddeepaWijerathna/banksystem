package com.company;

public enum AccountType {
    SAVINGS_ACCOUNT, CURRENT_ACCOUNT
}
