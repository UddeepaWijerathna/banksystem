package com.company;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        AccountHolder accountHolder1 = new AccountHolder(948411198, "Amali");
        Account account1 = new Account(1, AccountType.SAVINGS_ACCOUNT, accountHolder1);

        Account account2 = new Account(2, AccountType.CURRENT_ACCOUNT, accountHolder1);

        Transaction transaction1 = new Transaction(account1, TransactionType.DEPOSIT, 10000);
        Transaction transaction2 = new Transaction(account1, TransactionType.WITHDRAW, 5000);

        Transaction transaction3 = new Transaction(account2, TransactionType.WITHDRAW, 5000);
        Transaction transaction5 = new Transaction(account2, TransactionType.DEPOSIT, 5000);
        Transaction transaction6 = new Transaction(account2, TransactionType.WITHDRAW, 4000);

        Transaction transaction4 = new Transaction(account1, TransactionType.DEPOSIT, 10000);

        Thread thread1 = new Thread(transaction1);
        Thread thread2 = new Thread(transaction2);
        Thread thread3 = new Thread(transaction3);
        Thread thread4 = new Thread(transaction4);
        Thread thread5 = new Thread(transaction5);
        Thread thread6 = new Thread(transaction6);

        account1.displayBalance();
        account2.displayBalance();

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();


        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();
        thread5.join();
        thread6.join();

        account1.displayBalance();
        account2.displayBalance();


    }
}
