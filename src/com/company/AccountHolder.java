package com.company;

public class AccountHolder {
    private int identityNumber;
    private String name;

    public AccountHolder(int identityNumber, String name) {
        this.identityNumber = identityNumber;
        this.name = name;
    }

    public int getIdentityNumber() {
        return identityNumber;
    }

    public String getName() {
        return name;
    }

    public void setIdentityNumber(int identityNumber) {
        this.identityNumber = identityNumber;
    }

    public void setName(String name) {
        this.name = name;
    }
}
