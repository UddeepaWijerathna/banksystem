package com.company;

public class Account {
    private final int accountNo;
    private final AccountType accountType;
    private double balance = 1000;
    private AccountHolder accountHolder;

    public Account(int accountNo, AccountType accountType, AccountHolder accountHolder) {
        this.accountNo = accountNo;
        this.accountType = accountType;
        this.accountHolder = accountHolder;
    }

    public int getAccountNo() {
        return accountNo;
    }

    public double getBalance() {
        return balance;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public AccountHolder getAccountHolder() {
        return accountHolder;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setAccountHolder(AccountHolder accountHolder) {
        this.accountHolder = accountHolder;
    }

    public void displayBalance() {
        System.out.println("The balance of your "+ this.getAccountType()+" is " + this.getBalance());
    }

    public synchronized void deposit(double amount) {
        double balance = this.getBalance() + amount;
        setBalance(balance);
        System.out.println("You have deposited " + amount + " /= successfully. Thank you.");
    }

    public synchronized void withdraw(double amount) {
        if(this.getBalance()>=amount) {
            double balance = this.getBalance() - amount;
            setBalance(balance);
            System.out.println("You have withdrawn " + amount + " /= successfully. Thank you.");
        }else{
            System.out.println("Your account balance is not enough to be withdrawn.");
        }
    }
}
