package com.company;

public class Transaction implements Runnable {

    private final Account account;
    private TransactionType transactionType;
    private double amount;

    public Transaction(Account account, TransactionType transactionType, double amount) {
        this.account = account;
        this.transactionType = transactionType;
        this.amount = amount;
    }

    public Account getAccount() {
        return account;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public void run() {
        if(transactionType == TransactionType.DEPOSIT) {
            account.deposit(this.getAmount());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else if(transactionType == TransactionType.WITHDRAW){
            account.withdraw(this.getAmount());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else
            System.out.println("Error");

    }
}
